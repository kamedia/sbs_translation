<?php

/**
 * @file
 * Codeception tests for this module.
 *
 * TODO: Write tests for...
 * - metatag editing
 * - summary field
 * - draft editing
 * - publish/unpublish page
 * - publish/unpublish translation
 * - deleting a translation of a page
 * - feedback form checkbox
 * - image uploading
 * - convert to use page objects
 * - repeat all tests in ALL interface languages.
 * - set default language to non-english
 * - do all tests as a WM or IM.
 */

use Page\CommonUsers as commonUsers;
use Page\Common as common;
use Page\Stepbystep as stepByStep;
use Page\EditSitePage as editSitePage;
use Codeception\Util\HttpCode as httpCode;

/**
 * Test class for this module.
 */
class Sbs3LanguageCest {
  private $language2 = 'aa';
  private $language3 = 'bn';

  /**
   * Steps to execute before each test.
   */
  public function _before(AcceptanceTester $I) {
    $this->commonUsers = new commonUsers($I);
    $this->common = new common($I);
    $this->stepByStep = new stepByStep($I);
    $this->editSitePage = new editSitePage($I);
    $this->httpCode = new httpCode($I);

    $I->login(ADMIN_USERNAME, ADMIN_PASSWORD);

    $I->amGoingTo('Add languages');
    $I->amOnPage('/admin/config/regional/language');
    $langs = $I->grabMultiple('#language-order input[type="radio"]', 'value');
    if (!in_array($this->language2, $langs)) {
      $I->amOnPage('/admin/config/regional/language/add');
      $I->selectOption('#edit-langcode', $this->language2);
      $I->click('Add language');
      $I->see('has been created and can now be used');
    }
    if (!in_array($this->language3, $langs)) {
      $I->amOnPage('/admin/config/regional/language/add');
      $I->selectOption('#edit-langcode', $this->language3);
      $I->click('Add language');
      $I->see('has been created and can now be used');
    }
    if (count($langs) > 3) {
      $I->see('Too many languages in test site. Must have only three');
    }
  }

  /**
   * Steps to execute after each test.
   */
  public function _after(AcceptanceTester $I) {
  }

  /**
   * Create page in three languages.
   */
  public function createThree(AcceptanceTester $I) {
    $I->wantTo('Create page in three languages');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in English and language2');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node id');
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add language3 translations.');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3");
    $I->fillField("#edit-body-$language3-0-value", "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that menu item is created and translated');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $this->common->dontSeePHPError();
    $I->click("//td/a[contains(text(), '$rand-title-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title-en", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language2", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language3", 'form#i18n-string-translate-page-overview-form');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path is created.');
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-en", 'table');
    $I->see("$rand-title-$language2", 'table');
    $I->see("$rand-title-$language3", 'table');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en", 'h1');
    $I->see("$rand-content-en", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->see("$rand-title-$language3", 'h1');
    $I->see("$rand-content-$language3", 'div#content');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function createOther(AcceptanceTester $I) {
    $I->wantTo('Create page in non-default language only');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in language2 (not the site default).');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language2 has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Check that menu item is created');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->see("$rand-title-$language2");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path is created');
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-$language2", 'table');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function addOtherA(AcceptanceTester $I) {
    $I->wantTo('Create page in non-default language and then add other translations using the English interface');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in language2 (not the site default)');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language2 has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add English and language3 translations');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-en-0-value", "$rand-title-en");
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3");
    $I->fillField("#edit-body-en-0-value", "$rand-content-en");
    $I->fillField("#edit-body-$language3-0-value", "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that menu item is created and translated');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $this->common->dontSeePHPError();
    $I->click("//td/a[contains(text(), '$rand-title-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title-$language2", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-en", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language3", 'form#i18n-string-translate-page-overview-form');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path is created');
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-en", 'table');
    $I->see("$rand-title-$language2", 'table');
    $I->see("$rand-title-$language3", 'table');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en", 'h1');
    $I->see("$rand-content-en", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->see("$rand-title-$language3", 'h1');
    $I->see("$rand-content-$language3", 'div#content');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function addOtherB(AcceptanceTester $I) {
    $I->wantTo('Create page in non-default language3, switch to language3 interface and add the other two translations');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in language3 (not the site default)');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language3);
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language3");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language3 has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add English and language2 translations using language3 interface');
    $I->amOnPage("/$language3/node/$node/editsbs/add/en/$language2");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-en-0-value", "$rand-title-en");
    $I->fillField("#edit-title-field-$language2-0-value", "$rand-title-$language2");
    $I->fillField("#edit-body-en-0-value", "$rand-content-en");
    $I->fillField("#edit-body-$language2-0-value", "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language3", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that menu item is created and translated');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->click("//td/a[contains(text(), '$rand-title-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title-en", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language2", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language3", 'form#i18n-string-translate-page-overview-form');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path is created');
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-en", 'table');
    $I->see("$rand-title-$language2", 'table');
    $I->see("$rand-title-$language3", 'table');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en", 'h1');
    $I->see("$rand-content-en", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->see("$rand-title-$language3", 'h1');
    $I->see("$rand-content-$language3", 'div#content');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function addOtherC(AcceptanceTester $I) {
    $I->wantTo('Create page in non-default language3, switch to language2 interface and add the other two translations');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in language3 (not the site default)');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language3);
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language3");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language3 has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add English and language2 translations');
    $I->amOnPage("/$language2/node/$node/editsbs/add/en/$language2");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-en-0-value", "$rand-title-en");
    $I->fillField("#edit-title-field-$language2-0-value", "$rand-title-$language2");
    $I->fillField("#edit-body-en-0-value", "$rand-content-en");
    $I->fillField("#edit-body-$language2-0-value", "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language2", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that menu item is created and translated');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->click("//td/a[contains(text(), '$rand-title-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title-en", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language2", 'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language3", 'form#i18n-string-translate-page-overview-form');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path is created');
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-en", 'table');
    $I->see("$rand-title-$language2", 'table');
    $I->see("$rand-title-$language3", 'table');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en", 'h1');
    $I->see("$rand-content-en", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->see("$rand-title-$language3", 'h1');
    $I->see("$rand-content-$language3", 'div#content');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function customMenu(AcceptanceTester $I) {
    $I->wantTo('ensure custom menu stays custom for third translation');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in English and language2');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->uncheckOption('#edit-menu-automenu');
    $I->fillField('#edit-left-link-title', "$rand-custommenu-en");
    $I->fillField('#edit-menu-link-title', "$rand-custommenu-$language2");
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add language3 translation');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3");
    $I->fillField("#edit-body-$language3-0-value", "$rand-content-$language3");
    $I->dontSeeCheckboxIsChecked('#edit-menu-automenu');
    $I->fillField('#edit-menu-link-title', "$rand-custommenu-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that menu translations are correct');
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $this->common->dontSeePHPError();
    $I->click("//td/a[contains(text(), '$rand-custommenu-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-custommenu-en", '#i18n-string-translate-page-overview-form');
    $I->see("$rand-custommenu-$language2", '#i18n-string-translate-page-overview-form');
    $I->see("$rand-custommenu-$language3", '#i18n-string-translate-page-overview-form');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function createThreeUploadFile(AcceptanceTester $I) {
    $I->wantTo('Create page in three languages and upload a file');
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in English and language2');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->attachFile('input#edit-field-file-und-0-upload', 'test.pdf');
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add language3 translations');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3");
    $I->fillField("#edit-body-$language3-0-value", "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that path actually works and titles are correct');
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en", 'h1');
    $I->see("$rand-content-en", 'div#content');
    $I->see('test.pdf');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2", 'h1');
    $I->see("$rand-content-$language2", 'div#content');
    $I->see('test.pdf');
    $this->common->dontSeePHPError();
    $I->amOnPage("/$language3/$rand-title-$language3");
    $I->see("$rand-title-$language3", 'h1');
    $I->see("$rand-content-$language3", 'div#content');
    $I->see('test.pdf');
    $this->common->dontSeePHPError();
  }

  /**
   * See $I->wantTo().
   */
  public function checkLangDir1(AcceptanceTester $I) {
    $I->wantTo('Check that when 2nd language is RTL, the page edit form fields have the correct lang and dir attributes');
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Change Afar to RTL');
    $I->amOnPage('/admin/config/regional/language/edit/aa');
    $I->selectOption('input[name=direction]', 1);
    $I->click('#edit-submit');

    $I->amGoingTo('Change English to be first');
    $I->amOnPage('/admin/config/regional/language');
    $I->selectOption('#edit-weight-en', "-10");
    $I->click('#edit-submit');

    $I->amGoingTo('Check field lang and dir attributes for new page form');
    $I->amOnPage('/node/add/wildfire-page');
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    // For 3+ lang sites, the lang and dir attribute is set with js.
    // $I->seeElement('#edit-title-field-und-0-value[lang="aa"][dir="rtl"]');
    // $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    // $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    // $I->seeElement('#edit-body-und-0-value[lang="aa"][dir="rtl"]');
    // $I->seeElement('#edit-body-und-0-summary[lang="aa"][dir="rtl"]');.
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-aa");
    $I->click('#edit-submit');

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo('Check field lang and dir attributes for SBS edit form');
    $I->amOnPage("/node/$nodes[0]/editsbs/aa");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');

    $I->amGoingTo('Check field lang and dir attributes for edit english page form');
    $I->amOnPage("/node/$nodes[0]/edit");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->amGoingTo('Check field lang and dir attributes for edit afar page form');
    $I->amOnPage("/node/$nodes[0]/edit/aa");
    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');
  }

  /**
   * See $I->wantTo().
   */
  public function singleColumnEdit(AcceptanceTester $I) {
    $I->wantTo("ensure use of single language node editing screens doesn't cause a mix up of title and menu link title translations");
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in English and language2');
    $I->amOnPage('/node/add/wildfire-page');
    $this->common->dontSeePHPError();
    $I->selectOption('#edit-language', $language2);
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->click('#edit-submit');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$rand-title-en");
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Add language3 translation');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3");
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->click('#edit-submit');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Edit language3 translation in single language screen, while in language2.');
    $I->amOnPage("/$language2/node/$node/edit/$language3");
    $I->uncheckOption('#edit-menu-automenu');
    $I->fillField('#edit-menu-link-title', "$rand-custommenu-$language3");
    $I->click('#edit-submit');
    $this->common->dontSeePHPError();

    $I->amOnPage("/$language2/node/$node");
    $I->dontSee("$rand-title-en", 'h1');
    $I->dontSee("$rand-title-$language3", 'h1');
    $I->dontSee("$rand-title-en", '#main-menu');
    $I->dontSee("$rand-title-$language3", '#main-menu');
    $I->dontSee("$rand-custommenu-$language3", '#main-menu');

    $I->amOnPage("/$language3/node/$node");
    $I->dontSee("$rand-title-en", 'h1');
    $I->dontSee("$rand-title-$language2", 'h1');
    $I->dontSee("$rand-title-en", '#main-menu');
    $I->dontSee("$rand-title-$language2", '#main-menu');

    $I->amOnPage("/node/$node");
    $I->dontSee("$rand-title-$language2", 'h1');
    $I->dontSee("$rand-title-$language3", 'h1');
    $I->dontSee("$rand-title-$language2", '#main-menu');
    $I->dontSee("$rand-title-$language3", '#main-menu');
    $I->dontSee("$rand-custommenu-$language3", '#main-menu');

    $I->amGoingTo('Edit language2 translation in single language screen, while in default language.');
    $I->amOnPage("/node/$node/edit/$language2");
    $I->fillField('#edit-title-field-aa-0-value', "$rand-editedtitle-$language2");
    $I->checkOption('#edit-menu-automenu');
    $I->click('#edit-submit');
    $this->common->dontSeePHPError();

    $I->amOnPage("/$language2/node/$node");
    $I->dontSee("$rand-title-en", 'h1');
    $I->dontSee("$rand-title-$language2", 'h1');
    $I->see("$rand-editedtitle-$language2", 'h1');
    $I->dontSee("$rand-title-$language3", 'h1');
    $I->dontSee("$rand-title-en", '#main-menu');
    $I->dontSee("$rand-title-$language2", '#main-menu');
    $I->see("$rand-editedtitle-$language2", '#main-menu');
    $I->dontSee("$rand-title-$language3", '#main-menu');
    $I->dontSee("$rand-custommenu-$language3", '#main-menu');

    $I->amOnPage("/$language3/node/$node");
    $I->dontSee("$rand-title-en", 'h1');
    $I->dontSee("$rand-title-$language2", 'h1');
    $I->dontSee("$rand-editedtitle-$language2", 'h1');
    $I->see("$rand-title-$language3", 'h1');
    $I->dontSee("$rand-title-en", '#main-menu');
    $I->dontSee("$rand-title-$language2", '#main-menu');
    $I->dontSee("$rand-editedtitle-$language2", '#main-menu');
    $I->see("$rand-title-$language3", '#main-menu');
    $I->dontSee("$rand-custommenu-$language3", '#main-menu');

    $I->amOnPage("/node/$node");
    $I->see("$rand-title-en", 'h1');
    $I->dontSee("$rand-title-$language2", 'h1');
    $I->dontSee("$rand-editedtitle-$language2", 'h1');
    $I->dontSee("$rand-title-$language3", 'h1');
    $I->see("$rand-title-en", '#main-menu');
    $I->dontSee("$rand-title-$language2", '#main-menu');
    $I->dontSee("$rand-editedtitle-$language2", '#main-menu');
    $I->dontSee("$rand-title-$language3", '#main-menu');
    $I->dontSee("$rand-custommenu-$language3", '#main-menu');
  }

  /**
   * See $I->wantTo().
   */
  public function checkThreeMenuWithSpaces(AcceptanceTester $I) {
    $I->wantTo("Test auto menu checkbox isn't fazed by surplus spaces in the title, with three languages.");
    $language2 = $this->language2;
    $language3 = $this->language3;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Create a page in 2 languages');
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new', 'h1');
    $this->common->dontSeePHPError();
    $I->fillField('#edit-title-field-en-0-value', " $rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', " $rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->common->dontSeePHPError();

    $I->amGoingTo('Get the node #');
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found', 'h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amGoingTo('Check that the auto menu box is still checked');
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->common->dontSeePHPError();
    $I->seeCheckboxIsChecked('#edit-menu-automenu');

    $I->amGoingTo('Add language3 translation');
    $I->amOnPage("/node/$node/editsbs/add/en/$language3");
    $this->common->dontSeePHPError();
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->fillField("#edit-title-field-$language3-0-value", "$rand-title-$language3 ");
    $I->fillField("#edit-body-$language3-0-value", "$rand-content-$language3");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $this->common->dontSeePHPError();

    $I->amGoingTo('Check that the auto menu box is still checked');
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->common->dontSeePHPError();
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->amOnPage("/node/$node/editsbs/$language3");
    $this->common->dontSeePHPError();
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
  }

}
