/**
 * @file
 *
 * I tried to do this all with drupal form api #states, but had quirky
 * behaviour with chained events.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.sbs_translation = {
    attach: function (context, settings) {

      // Attach change event to pathauto checkbox. If it's (un)checked, toggle
      // the display of path alias field(s).
      $('#edit-path-pathauto').once().change(function(event) {
        if($(this).attr('checked')) {
          $('#edit-path').hide();
          $('#edit-sbspath').hide();
        } else {
          $('#edit-path').show();
          $('#edit-sbspath').show();
        }
      });

      // Make sure change event for pathauto checkbox fires when user types in
      // title field.
      $('input[id^=edit-title-field]').once().keyup(function(event) {
        $('#edit-path-pathauto').change();
      });

      // Trigger change event on document ready.
      $('#edit-path-pathauto').once('sbs_translation', function () {
        $(this).trigger("change");
      });

      // Move the required asterisk to the fieldset legend
      $("#edit-field-image span.form-required").once().appendTo($("#edit-field-image legend>span"));
      $("#edit-field-file span.form-required").once().appendTo($("#edit-field-file legend>span"));
      $("#edit-field-pdf span.form-required").once().appendTo($("#edit-field-pdf legend>span"));

      // Move the filefield_sources plupload "Start upload" button into plupload
      // footer to resemble scald (normal) plupload.
      $(".filefield-source-plupload input[type='submit']").appendTo($("form.node-form .plupload_buttons"));

      // Hide the non-js Drupal upload field.
      //~$("input[type='file'][id^='edit-field']").remove();
      //~$("input[type='submit'][name$='upload_button']").remove();

      // If the language selector is visible, (ie. the site has 3+ languages and
      // the user is creating a new page), change the HTML lang and dir
      // attributes on the right-hand side fields to match the lang selector.
      if(typeof settings.sbs !== "undefined") {
        $("#edit-language", context).change(function () {
          var lang = $("#edit-language").val();
          var langindex = settings.sbs.lang.indexOf(lang);
          var dir = settings.sbs.dir[langindex];
          $("#edit-title-field-und-0-value").attr({lang: lang, dir: dir});
          $("#edit-menu-link-title").attr({lang: lang, dir: dir});
          $("#edit-path-alias").attr({lang: lang, dir: dir});
          $("#edit-body-und-0-value").attr({lang: lang, dir: dir});
          $("#edit-body-und-0-summary").attr({lang: lang, dir: dir});
          $("#edit-field-body2-und-0-value").attr({lang: lang, dir: dir});
          $("#edit-metatags-und-title-value").attr({lang: lang, dir: dir});
          $("#edit-metatags-und-description-value").attr({lang: lang, dir: dir});
          $("#edit-body iframe").contents().find("html").attr({lang: lang, dir: dir});
          $("#edit-body iframe").contents().find("body").removeClass(function (index, className) {
            return (className.match (/wildfire-font-language-../g) || []).join(' ');
          }).addClass('wildfire-font-language-' + lang);
          $("#edit-body iframe").contents().find("body").removeClass(function (index, className) {
            return (className.match (/cke_contents_.../g) || []).join(' ');
          }).addClass('cke_contents_' + dir);
          $("#edit-field-body2--2 iframe").contents().find("html").attr({lang: lang, dir: dir});
          $("#edit-field-body2--2 iframe").contents().find("body").removeClass(function (index, className) {
            return (className.match (/wildfire-font-language-../g) || []).join(' ');
          }).addClass('wildfire-font-language-' + lang);
          $("#edit-field-body2--2 iframe").contents().find("body").removeClass(function (index, className) {
            return (className.match (/cke_contents_.../g) || []).join(' ');
          }).addClass('cke_contents_' + dir);
        })
        .change();

        // Add a form submit event to confirm the language selector.
        $("form.node-form", context).submit(function(event) { // nope
          if($('#edit-title-field-und-0-value').val()) {
            var selected_language = $("#edit-language option:selected").text();
            return confirm(Drupal.t('Language selector is set to @language. Are you sure?', {'@language': selected_language}));
          }
        });
      }
    }
  };

}(jQuery));
