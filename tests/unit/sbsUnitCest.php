<?php

//require('sbs_translation.module');

class sbsUnitCest
{
  public function _before(UnitTester $I)
  {
  }

  public function _after(UnitTester $I)
  {
  }

  // tests
  public function implements_alter(UnitTester $I)
  {
    $implementations = array('background_and_banner' => false, 'captcha' => false, 'date_popup_authored' => false, 'diff' => false, 'l10n_update' => false, 'locale' => false, 'mee' => false, 'menu' => false, 'metatag' => false, 'path' => false, 'save_draft' => false, 'scald_fcbh' => false, 'scald_galleria' => false, 'scald_kaltura' => false, 'scald_playlist' => false, 'scald_wildfire' => false, 'stepbystep' => false, 'subform' => false, 'token' => false, 'total_control' => false, 'views_bulk_operations' => false, 'wf_interface_scald' => false, 'xmlsitemap_node' => false, 'local_fonts' => false, 'pathauto' => false, 'clientside_validation' => false, 'i18n_menu' => false, 'entity_translation' => false, 'sbs_translation' => false, 'variable' => false,);

    $I->wantTo('SBS is at the end of implements_alter');
    sbs_translation_module_implements_alter($implementations, 'form_alter');
    $asdf = end($implementations);
    $I->assertEquals('sbs_translation', key($implementations));

    sbs_translation_module_implements_alter($implementations, 'menu_alter');
    $asdf = end($implementations);
    $I->assertEquals('sbs_translation', key($implementations));

    sbs_translation_module_implements_alter($implementations, 'entity_presave');
    $asdf = end($implementations);
    $I->assertEquals('sbs_translation', key($implementations));

    sbs_translation_module_implements_alter($implementations, 'menu_local_tasks_alter');
    $asdf = end($implementations);
    $I->assertEquals('sbs_translation', key($implementations));
  }

/*
  public function checkMenuAlters(AcceptanceTester $I)
  {
    //$I->wantTo('Check menu alters');
    //$this->_login($I);
    $items = array();
    $items['node/%node/translate']['access callback'] = 'asdf';
    $items['node/%node/translate']['weight'] = 1;
    $items['node/%node/revisions']['weight'] = 2;
    sbs_translation_menu_alter($items);
    $I->assertEquals('sbs_translation_menu_translate_tab_access_callback', $items['node/%node/translate']);
  }
*/
}
