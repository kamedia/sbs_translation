<?php
/*
 * TODO: Write tests for...
 * - metatag editing
 * - summary field
 * - draft editing
 * - publish/unpublish page
 * - publish/unpublish translation
 * - deleting a translation of a page
 * - feedback form checkbox
 * - image uploading
 * - convert to use page objects
 * - set default language to non-english
 * - do all tests as a WM or IM
 */

class sbs2LanguageCest
{

  private $language2 = 'aa';

  // Check for PHP warnings and notices.
  private function dontSeePHPError(AcceptanceTester $I)
  {
    $I->dontSee('Warning:', '.messages.error');
    $I->dontSee('Notice:', '.messages.error');
  }

  // HOOK: before test
  public function _before(AcceptanceTester $I)
  {
    $I->login(ADMIN_USERNAME, ADMIN_PASSWORD);
    // Add second language.
    $I->amOnPage('/admin/config/regional/language/add');
    $I->selectOption('#edit-langcode', $this->language2);
    $I->click('Add language');
    $I->see('has been created and can now be used');
  }

  // HOOK: after test
  public function _after(AcceptanceTester $I)
  {
  }

  public function checkSBSCreate(AcceptanceTester $I)
  {
    $I->wantTo('Test two language page creation');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that menu item is created.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $this->dontSeePHPError($I);
    $I->click("//td/a[contains(text(), '$rand-title-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title-en",'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title-$language2",'form#i18n-string-translate-page-overview-form');
    $this->dontSeePHPError($I);

    // Check that path is created.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title-en",'table');
    $I->see("$rand-title-$language2",'table');
    $this->dontSeePHPError($I);

    // Check that path actually works and titles are correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);
  }

  public function checkSBSEdit(AcceptanceTester $I)
  {
    $I->wantTo('Test two language page editing');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that titles can be customized.
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title2-en");
    $I->fillField("#edit-title-field-$language2-0-value", "$rand-title2-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title2-en has been updated.");
    $this->dontSeePHPError($I);
    $I->amOnPage("/$rand-title2-en");
    $I->see("$rand-title2-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title2-$language2");
    $I->see("$rand-title2-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);

    // Check that menu item has been updated to match title.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->click("//td/a[contains(text(), '$rand-title2-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-title2-en",'form#i18n-string-translate-page-overview-form');
    $I->see("$rand-title2-$language2",'form#i18n-string-translate-page-overview-form');
    $this->dontSeePHPError($I);

    // Check that path has been updated to match title.
    // NOTE: Normally, when you change the title, auto-path will un-check via JS
    // and the path will stay the same. But because PHPBrowser doesn't do JS,
    // auto-path will stay checked, and the path should match the title.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title2-en",'table');
    $I->see("$rand-title2-$language2",'table');

    // Check that path actually works and titles are correct.
    $I->amOnPage("/$rand-title2-en");
    $I->see("$rand-title2-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title2-$language2");
    $I->see("$rand-title2-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);
  }

  public function checkSBSPath(AcceptanceTester $I)
  {
    $I->wantTo('Test two language path customizing');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Customize the path.
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->uncheckOption('#edit-path-pathauto');
    $I->fillField('#edit-left-alias', "$rand-title2-en");
    $I->fillField('#edit-path-alias', "$rand-title2-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been updated.");
    $this->dontSeePHPError($I);

    // Check that path is correct.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title2-en",'table');
    $I->see("$rand-title2-$language2",'table');
    $this->dontSeePHPError($I);

    // Check that path actually works and titles are correct.
    $I->amOnPage("/$rand-title2-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title2-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);
  }

  public function checkSBSMenu(AcceptanceTester $I)
  {
    $I->wantTo('Test two language menu customizing');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Customized the menu link.
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->uncheckOption('#edit-menu-automenu');
    $I->fillField('#edit-left-link-title', "$rand-menu-en");
    $I->fillField('#edit-menu-link-title', "$rand-menu-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been updated.");
    $this->dontSeePHPError($I);

    // Check that menu links are correct.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->click("//td/a[contains(text(), '$rand-menu-en')]/../../td/a[contains(text(), 'translate')]");
    $I->see("$rand-menu-en");
    $I->see("$rand-menu-$language2");
    $this->dontSeePHPError($I);

    // Check that titles are still correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);
  }


  public function checkSBSMenuRight(AcceptanceTester $I)
  {
    $I->wantTo('Test two language with custom menu on right');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    // Customize the right language menu link.
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
    $I->uncheckOption('#edit-menu-automenu');
    $I->fillField('#edit-left-link-title', "$rand-title-en");
    $I->fillField('#edit-menu-link-title', "$rand-custommenu-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that automenu is off.
    $I->amOnPage("/node/$node/editsbs/$language2");
    $I->dontSeeCheckboxIsChecked('#edit-menu-automenu');
    $this->dontSeePHPError($I);

    // Check that menu links are correct.
    //~ $I->amOnPage('/admin/structure/menu/manage/main-menu');
    //~ $I->click("//td/a[contains(text(), '$rand-menu-en')]/../../td/a[contains(text(), 'translate')]");
    //~ $I->see("$rand-title-en", '#edit-left-link-title');
    $I->seeInField('#edit-left-link-title', "$rand-title-en");
    $I->seeInField('#edit-menu-link-title', "$rand-custommenu-$language2");
    //~ $I->see(, '#edit-sbsmenu');
  }

  public function checkSBSLeftFirst(AcceptanceTester $I)
  {
    $I->wantTo('Test SBS left, then right');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in English only.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Add language2 to English page.
    $I->amOnPage("/node/$node/editsbs/add/en/$language2");
    $this->dontSeePHPError($I);
    $I->fillField("#edit-title-field-$language2-0-value", "$rand-title-$language2");
    $I->fillField("#edit-body-$language2-0-value", "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been updated.");
    $this->dontSeePHPError($I);

    // Check that titles and translations are correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);

    // Check that both languages are published.
    $I->amOnPage("node/$node/translate");
    $I->dontSee('Not Published', 'tr');
    $this->dontSeePHPError($I);
  }

  public function checkSBSRightFirst(AcceptanceTester $I)
  {
    $I->wantTo('Test SBS right, then left');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in language2 only.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language2 has been created.");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Add English to language2 page.
    $I->amOnPage("/$language2/node/$node/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->click('#edit-submit');
    $I->see("$rand-title-$language2 has been updated.");
    $I->see("$rand-content-$language2");
    $this->dontSeePHPError($I);

    // Check that titles and translations are correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $this->dontSeePHPError($I);

    // Check that both languages are published.
    $I->amOnPage("node/$node/translate");
    $I->dontSee('Not Published', 'tr');
    $this->dontSeePHPError($I);
  }

  public function checkSBSParent(AcceptanceTester $I)
  {
    $I->wantTo('Test two language parent menu');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title1-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title1-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content1-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content1-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title1-en has been created.");
    $I->see("$rand-content1-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title1-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Create another page and change the parent menu item.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title2-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title2-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content2-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content2-$language2");
    $I->selectOption('#edit-menu-parent', "-- $rand-title1-en");
    $I->click('#edit-submit');
    $I->see("$rand-title2-en has been created.");
    $I->see("$rand-content2-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title1-en/$rand-title2-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node2 = end($patharray);

    // Check the parent menu item.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title1-en/$rand-title2-en");
    $I->see("$rand-title1-$language2/$rand-title2-$language2");
    $this->dontSeePHPError($I);

    // Check that titles and translations are correct.
    $I->amOnPage("/$rand-title1-en/$rand-title2-en");
    $I->see("$rand-title2-en",'h1');
    $I->see("$rand-content2-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title1-$language2/$rand-title2-$language2");
    $I->see("$rand-title2-$language2",'h1');
    $I->see("$rand-content2-$language2",'div#content');
    $this->dontSeePHPError($I);
  }

  // This test is also testing pathauto and token.
  public function checkSBSParentTwoDeep(AcceptanceTester $I)
  {
    $I->wantTo('Test two language parent menu two levels deep');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title1-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title1-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content1-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content1-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title1-en has been created.");
    $I->see("$rand-content1-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title1-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Create another page and change the parent menu item.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title2-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title2-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content2-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content2-$language2");
    $I->selectOption('#edit-menu-parent', "-- $rand-title1-en");
    $I->click('#edit-submit');
    $I->see("$rand-title2-en has been created.");
    $I->see("$rand-content2-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title1-en/$rand-title2-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node2 = end($patharray);

    // Create another page and change at root level.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title3-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title3-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content3-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content3-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title3-en has been created.");
    $I->see("$rand-content3-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title3-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node3 = end($patharray);

    // Edit page #3 and change the parent.
    $I->amOnPage("/node/$node3/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->selectOption('#edit-menu-parent', "---- $rand-title2-en");
    $I->click('#edit-submit');
    $I->see("$rand-title3-en has been updated.");
    $I->see("$rand-content3-en");
    $this->dontSeePHPError($I);

    // Check the parent menu item.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title1-en/$rand-title2-en/$rand-title3-en");
    $I->see("$rand-title1-$language2/$rand-title2-$language2/$rand-title3-$language2");
    $this->dontSeePHPError($I);

    // Check that titles and translations are correct.
    $I->amOnPage("/$rand-title1-en/$rand-title2-en/$rand-title3-en");
    $I->see("$rand-title3-en",'h1');
    $I->see("$rand-content3-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title1-$language2/$rand-title2-$language2/$rand-title3-$language2");
    $I->see("$rand-title3-$language2",'h1');
    $I->see("$rand-content3-$language2",'div#content');
    $this->dontSeePHPError($I);
  }


  public function checkSBSTwoUploadFile(AcceptanceTester $I)
  {
    // Check that file uploading works.

    $I->wantTo('Test two language page with file uploading');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);
    $language2 = $this->language2;

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->attachFile('input#edit-field-file-und-0-upload', 'test.pdf');
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Check that path actually works and titles are correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $I->see('test.pdf');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    $I->see("$rand-content-$language2",'div#content');
    $I->see('test.pdf');
    $this->dontSeePHPError($I);
  }

  public function checkSBSNoBody2ndLanguage(AcceptanceTester $I)
  {
    $I->wantTo('Test two language page creation with blank body text in 2nd language');
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages but leave body blank in second language.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that path actually works and titles are correct.
    $I->amOnPage("/$rand-title-en");
    $I->see("$rand-title-en",'h1');
    $I->see("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
    $I->amOnPage("/$language2/$rand-title-$language2");
    $I->see("$rand-title-$language2",'h1');
    // If body text is blank, Drupal/entity_translation standard behavior is to
    // display the fallback language body text. But sbs should prevent this.
    $I->dontSee("$rand-content-en",'div#content');
    $this->dontSeePHPError($I);
  }

  public function checkSBSPreview(AcceptanceTester $I, Page\CommonUsers $commonUsers)
  {
    $I->wantTo("Check that a page preview is shown when an IM creates/edits a page and saves");
    $language2 = $this->language2;

    $commonUsers->createUserWithRole('im', 'Implementation manager');
    $commonUsers->loginUser('im');

    $I->amGoingTo("Create a page in 2 languages and click 'Save and publish'");
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en", 'h1');
    $I->seeInCurrentUrl("$rand-title-en");
    $I->dontSee("Overview", 'h1');
    $I->dontSee("Welcome to Wildfire", 'h1');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo("Edit the page and click 'Save and unpublish'");
    $I->amOnPage("/node/$nodes[0]/editsbs/$language2");
    $I->click('#edit-draft');
    $I->see("$rand-title-en", 'h1');
    $I->seeInCurrentUrl("$rand-title-en");
    $I->dontSee("Overview", 'h1');
    $I->dontSee("Welcome to Wildfire", 'h1');
    $this->dontSeePHPError($I);

    $I->amGoingTo("Create a page in 2 languages and click 'Save as draft'");
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-$language2");
    $I->click('#edit-draft');
    $I->see("$rand-title-en", 'h1');
    $I->seeInCurrentUrl("$rand-title-en");
    $I->dontSee("Overview", 'h1');
    $I->dontSee("Welcome to Wildfire", 'h1');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo("Edit the page and click 'Save and publish'");
    $I->amOnPage("/node/$nodes[1]/editsbs/$language2");
    $I->click('#edit-draft');
    $I->see("$rand-title-en", 'h1');
    $I->seeInCurrentUrl("$rand-title-en");
    $I->dontSee("Overview", 'h1');
    $I->dontSee("Welcome to Wildfire", 'h1');
    $this->dontSeePHPError($I);
  }

  public function checkSBSLangDir1(AcceptanceTester $I, Page\CommonUsers $commonUsers)
  {
    $I->wantTo('Check that when 2nd language is RTL, the page edit form fields have the correct lang and dir attributes');
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Change Afar to RTL');
    $I->amOnPage('/admin/config/regional/language/edit/aa');
    $I->selectOption('input[name=direction]', 1);
    $I->click('#edit-submit');

    $I->amGoingTo('Change Afar to be below English');
    $I->amOnPage('/admin/config/regional/language');
    $I->selectOption('#edit-weight-aa', "10");
    $I->click('#edit-submit');

    $I->amGoingTo('Check field lang and dir attributes for new page form');
    $I->amOnPage('/node/add/wildfire-page');
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->seeElement('#edit-title-field-und-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-und-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-und-0-summary[lang="aa"][dir="rtl"]');

    $I->fillField('#edit-title-field-en-0-value', "$rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-aa");
    $I->click('#edit-submit');

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo('Check field lang and dir attributes for SBS edit form');
    $I->amOnPage("/node/$nodes[0]/editsbs/aa");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-left-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');

    $I->amGoingTo('Check field lang and dir attributes for edit english page form');
    $I->amOnPage("/node/$nodes[0]/edit");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->amGoingTo('Check field lang and dir attributes for edit afar page form');
    $I->amOnPage("/node/$nodes[0]/edit/aa");
    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');
  }

  public function checkSBSLangDir2(AcceptanceTester $I, Page\CommonUsers $commonUsers)
  {
    $I->wantTo('Check that when default language is RTL, the page edit form fields have the correct lang and dir attributes');
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Change Afar to RTL');
    $I->amOnPage('/admin/config/regional/language/edit/aa');
    $I->selectOption('input[name=direction]', 1);
    $I->click('#edit-submit');

    $I->amGoingTo('Make Afar the default language');
    $I->amOnPage('/admin/config/regional/language');
    $I->selectOption('input[name="site_default"]', array('value' => 'aa'));
    $I->click('#edit-submit');

    $I->amGoingTo('Check field lang and dir attributes for new page form');
    $I->amOnPage('/node/add/wildfire-page');
    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-left-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-left-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');

    $I->seeElement('#edit-title-field-und-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-und-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-und-0-summary[lang="en"][dir="ltr"]');

    $I->fillField('#edit-title-field-aa-0-value', "$rand-title-aa");
    $I->fillField('#edit-title-field-und-0-value', "$rand-title-en");
    $I->click('#edit-submit');

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo('Check field lang and dir attributes for SBS edit form');
    $I->amOnPage("/node/$nodes[0]/editsbs/en");
    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-left-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-left-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');

    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');

    $I->amGoingTo('Check field lang and dir attributes for edit english page form');
    $I->amOnPage("/node/$nodes[0]/edit");
    $I->seeElement('#edit-title-field-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-value[lang="aa"][dir="rtl"]');
    $I->seeElement('#edit-body-aa-0-summary[lang="aa"][dir="rtl"]');

    $I->amGoingTo('Check field lang and dir attributes for edit afar page form');
    $I->amOnPage("/node/$nodes[0]/edit/en");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="ltr"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="ltr"]');
  }

  public function checkSBSTwoMenuWithSpaces(AcceptanceTester $I)
  {
    $I->wantTo("Test the auto menu checkbox isn't fazed by surplus spaces in the title, with two languages.");
    $language2 = $this->language2;

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page in 2 languages.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', " $rand-title-en");
    $I->fillField('#edit-title-field-und-0-value', " $rand-title-$language2");
    $I->fillField('#edit-body-en-0-value', "$rand-content-en");
    $I->fillField('#edit-body-und-0-value', "$rand-content-$language2");
    $I->click('#edit-submit');
    $I->see("$rand-title-en has been created.");
    $I->see("$rand-content-en");
    $this->dontSeePHPError($I);

    // Get the node #.
    $I->amOnPage("/$rand-title-en");
    $I->dontSee('Page not found','h1');
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that the auto menu box is still checked.
    $I->amOnPage("/node/$node/editsbs/$language2");
    $this->dontSeePHPError($I);
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
  }

}
