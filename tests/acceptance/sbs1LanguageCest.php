<?php
/*
 * TODO: Write tests for...
 * - metatag editing
 * - summary field
 * - draft editing
 * - publish/unpublish
 * - deleting a translation of a page
 * - feedback form checkbox
 * - image uploading
 * - convert to use page objects
 * - do all tests as a WM or IM
 */

class sbs1LanguageCest
{

  // Check for PHP warnings and notices.
  private function dontSeePHPError(AcceptanceTester $I)
  {
    $I->dontSee('Warning:', '.messages.error');
    $I->dontSee('Notice:', '.messages.error');
  }

  // HOOK: before test
  public function _before(AcceptanceTester $I)
  {
    $I->login(ADMIN_USERNAME, ADMIN_PASSWORD);
  }

  // HOOK: after test
  public function _after(AcceptanceTester $I)
  {
  }

  public function checkSBSPermissions(AcceptanceTester $I)
  {
    $I->wantTo('Check SBS permissions');
    $I->amOnPage('/admin/people/permissions');
    $I->see('Use side-by-side editor');
  }

  public function checkSBSOneCreate(AcceptanceTester $I)
  {
    $I->wantTo('Test one language page creation');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that menu item is created.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->see("$rand-title");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOneEdit(AcceptanceTester $I)
  {
    $I->wantTo('Test one language page editing');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Modify the page.
    $I->amOnPage("/node/$node/edit");
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title2");
    $I->fillField('#edit-body-en-0-value', "$rand-content2");
    $I->click('#edit-submit');
    $I->see("$rand-title2 has been updated.");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Check that menu item has been updated to match title.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->see("$rand-title2");
    $this->dontSeePHPError($I);

    // Check that path has been updated to match title.
    // NOTE: Normally, when you change the title, auto-path will un-check via JS
    // and the path will stay the same. But because PHPBrowser doesn't do JS,
    // auto-path will stay checked, and the path should match the title.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title2");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOneMenu(AcceptanceTester $I)
  {
    $I->wantTo('Test one language menu customizing');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    $I->amOnPage("/node/$node/edit");
    $this->dontSeePHPError($I);
    $I->uncheckOption('#edit-menu-automenu');
    $I->fillField('#edit-menu-link-title', "$rand-menu");
    $I->click('#edit-submit');
    $I->see("$rand-title has been updated.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that menu item has been updated to match title.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $I->see("$rand-menu");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOnePath(AcceptanceTester $I)
  {
    $I->wantTo('Test one language path customizing');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Customize the path.
    $I->amOnPage("/node/$node/edit");
    $I->uncheckOption('#edit-path-pathauto');
    $I->fillField('#edit-path-alias', "$rand-path");
    $I->click('#edit-submit');
    $I->see("$rand-title has been updated.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path has been updated to match title.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-path");
    $this->dontSeePHPError($I);

   // Check that path actually works.
    $I->amOnPage("/$rand-path");
    $I->see("$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOneParent(AcceptanceTester $I)
  {
    $I->wantTo('Test one language parent menu');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title1");
    $I->fillField('#edit-body-und-0-value', "$rand-content1");
    $I->click('#edit-submit');
    $I->see("$rand-title1 has been created.");
    $I->see("$rand-content1");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title1");
    $I->see("$rand-content1");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Create another page and change the parent menu item.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title2");
    $I->fillField('#edit-body-und-0-value', "$rand-content2");
    $I->selectOption('#edit-menu-parent', "-- $rand-title1");
    $I->click('#edit-submit');
    $I->see("$rand-title2 has been created.");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title1/$rand-title2");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node2 = end($patharray);

    // Check the parent menu item.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title1/$rand-title2");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOneMetatags(AcceptanceTester $I)
  {
    $I->wantTo('Test one language metatag editing');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    $I->seeInTitle("$rand-title");
    $I->seeInSource('<meta name="description" content="' . $rand . '-content" />');
    $I->seeInSource('<meta property="og:title" content="' . $rand . '-title |');
    $I->seeInSource('<meta property="og:description" content="' . $rand . '-content" />');
    $I->seeInSource('<meta name="twitter:title" content="' . $rand . '-title |');
    $I->seeInSource('<meta name="twitter:description" content="' . $rand . '-content" />');

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Modify the page.
    $I->amOnPage("/node/$node/edit");
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field-en-0-value', "$rand-title2");
    $I->fillField('#edit-body-en-0-value', "$rand-content2");
    $I->fillField('#edit-body-en-0-summary', "$rand-summary2");
    $I->click('#edit-submit');
    $I->see("$rand-title2 has been updated.");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Check that metatags have been updated.
    $I->seeInTitle("$rand-title2");
    $I->seeInSource('<meta name="description" content="' . $rand . '-summary2" />');
    $I->seeInSource('<meta property="og:title" content="' . $rand . '-title2 |');
    $I->seeInSource('<meta property="og:description" content="' . $rand . '-summary2" />');
    $I->seeInSource('<meta name="twitter:title" content="' . $rand . '-title2 |');
    $I->seeInSource('<meta name="twitter:description" content="' . $rand . '-summary2" />');

    // Override the metatags with custom values.
    $I->amOnPage("/node/$node/edit");
    $this->dontSeePHPError($I);
    $I->fillField('#edit-metatags-en-title-value', "$rand-title-override");
    //$I->fillField('#edit-metatags-en-description-value', "$rand-description-override");
    $I->checkOption('#edit-metatags-en-robots-value-index');
    $I->checkOption('#edit-metatags-en-robots-value-follow');
    $I->checkOption('#edit-metatags-en-robots-value-noindex');
    $I->checkOption('#edit-metatags-en-robots-value-nofollow');
    $I->checkOption('#edit-metatags-en-robots-value-noarchive');
    $I->checkOption('#edit-metatags-en-robots-value-nosnippet');
    $I->checkOption('#edit-metatags-en-robots-value-noodp');
    $I->checkOption('#edit-metatags-en-robots-value-noydir');
    $I->checkOption('#edit-metatags-en-robots-value-noimageindex');
    $I->checkOption('#edit-metatags-en-robots-value-notranslate');
    $I->click('#edit-submit');
    $I->see("$rand-title2 has been updated.");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Check that metatags have been updated.
    $I->seeInTitle("$rand-title-override");
    $I->seeInSource('<meta name="robots" content="follow, index, noarchive, nofollow, noimageindex, noindex, noodp, nosnippet, notranslate, noydir" />');
    //$I->seeInSource('<meta name="description" content="' . $rand . '-description-override" />');
    //$I->seeInSource('<meta property="og:title" content="' . $rand . '-title-override" />');
    //$I->seeInSource('<meta property="og:description" content="' . $rand . '-description-override" />');
    //$I->seeInSource('<meta name="twitter:title" content="' . $rand . '-title-override" />');
    //$I->seeInSource('<meta name="twitter:description" content="' . $rand . '-description-override" />');
  }

  public function checkSBSOneParentTwoDeep(AcceptanceTester $I)
  {
    $I->wantTo('Test one language parent menu two levels deep');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title1");
    $I->fillField('#edit-body-und-0-value', "$rand-content1");
    $I->click('#edit-submit');
    $I->see("$rand-title1 has been created.");
    $I->see("$rand-content1");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Create another page and change the parent menu item.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title2");
    $I->fillField('#edit-body-und-0-value', "$rand-content2");
    $I->selectOption('#edit-menu-parent', "-- $rand-title1");
    $I->click('#edit-submit');
    $I->see("$rand-title2 has been created.");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title1/$rand-title2");
    $I->see("$rand-content2");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node2 = end($patharray);

    // Create another page and change the parent menu item.
    $I->amOnPage('/node/add/wildfire-page');
    $this->dontSeePHPError($I);
    $I->fillField('#edit-title-field input', "$rand-title3");
    $I->fillField('#edit-body-und-0-value', "$rand-content3");
    $I->selectOption('#edit-menu-parent', "---- $rand-title2");
    $I->click('#edit-submit');
    $I->see("$rand-title3 has been created.");
    $I->see("$rand-content3");
    $this->dontSeePHPError($I);

    // Check that path actually works.
    $I->amOnPage("/$rand-title1/$rand-title2/$rand-title3");
    $I->see("$rand-content3");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node3 = end($patharray);

    // Check the parent menu item.
    $I->amOnPage('/admin/config/search/path');
    $I->see("$rand-title1/$rand-title2/$rand-title3");
    $this->dontSeePHPError($I);
  }

  public function checkSBSOneMenuWeightNew(AcceptanceTester $I)
  {
    // Drupal gives new pages a weight of 50, then sorts them alphabetically.
    // Wildfire changes new pages to be at the bottom of their siblings in menu.

    $I->wantTo('Test one language menu weight ordering');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-title2");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    // Create another page that is alphabetically higher than first page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-title1");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    // Check that second page menu link appears below first page.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $weight_page1 = $I->grabValueFrom("//td/a[contains(text(), '$rand-title2')]/../../td/div/select");
    $I->seeOptionIsSelected("//td/a[contains(text(), '$rand-title1')]/../../td/div/select", $weight_page1 + 1);
  }

  public function checkSBSOneMenuWeightEdit(AcceptanceTester $I)
  {
    // Drupal gives new pages a weight of 50, then sorts them alphabetically.
    // Wildfire changes new pages to be at the bottom of their siblings in menu.

    $I->wantTo('Test one language menu weight for pages that are moved to a new parent');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create page1 at root level.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-page1");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    // Create page2 at root level.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-page2");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node2 = end($patharray);

    // Create page3 as a child of page1.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-page3");
    $I->selectOption('#edit-menu-parent', "-- $rand-page1");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node3 = end($patharray);

    // Move page2 to be a child of page1.
    $I->amOnPage("/node/$node2/edit");
    $I->selectOption('#edit-menu-parent', "-- $rand-page1");
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Check that page2 menu link appears below page3.
    $I->amOnPage('/admin/structure/menu/manage/main-menu');
    $weight_page1 = $I->grabValueFrom("//td/a[contains(text(), '$rand-page3')]/../../td/div/select");
    $I->seeOptionIsSelected("//td/a[contains(text(), '$rand-page2')]/../../td/div/select", $weight_page1 + 1);
  }

  public function checkSBSOneUploadFile(AcceptanceTester $I)
  {
    // Check that file uploading works.

    $I->wantTo('Test one language page with file uploading');

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create page1 at root level.
    $I->amOnPage('/node/add/wildfire-page');
    $I->fillField('#edit-title-field input', "$rand-title");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->attachFile('input#edit-field-file-und-0-upload', 'test.pdf');
    $I->click('#edit-submit');
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node1 = end($patharray);

    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $I->see('test.pdf');
  }

  public function checkSBSLangDir1(AcceptanceTester $I, Page\CommonUsers $commonUsers)
  {
    $I->wantTo('Check that when language is RTL, the page edit form fields have correct lang and dir attributes');
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo('Change English to RTL (hee hee)');
    $I->amOnPage('/admin/config/regional/language/edit/en');
    $I->selectOption('input[name=direction]', 1);
    $I->click('#edit-submit');

    $I->amGoingTo('Check field lang and dir attributes for new page form');
    $I->amOnPage('/node/add/wildfire-page');

    $I->seeElement('#edit-title-field-und-0-value[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-body-und-0-value[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-body-und-0-summary[lang="en"][dir="rtl"]');

    $I->fillField('#edit-title-field-und-0-value', "$rand-title-aa");
    $I->click('#edit-submit');

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $nodes[] = end($patharray);

    $I->amGoingTo('Check field lang and dir attributes for edit english page form');
    $I->amOnPage("/node/$nodes[0]/edit");
    $I->seeElement('#edit-title-field-en-0-value[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-menu-link-title[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-path-alias[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-body-en-0-value[lang="en"][dir="rtl"]');
    $I->seeElement('#edit-body-en-0-summary[lang="en"][dir="rtl"]');
  }

  public function checkSBSOneMenuWithSpaces(AcceptanceTester $I)
  {
    $I->wantTo("Test the auto menu checkbox isn't fazed by surplus spaces in the title.");

    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    // Create a page.
    $I->amOnPage('/node/add/wildfire-page');
    $I->see('Add new','h1');
    $this->dontSeePHPError($I);
    // Use a page title with excessive spaces.
    $I->fillField('#edit-title-field input', " $rand-title ");
    $I->fillField('#edit-body-und-0-value', "$rand-content");
    $I->click('#edit-submit');
    $I->see("$rand-title has been created.");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Check that path works.
    $I->amOnPage("/$rand-title");
    $I->see("$rand-content");
    $this->dontSeePHPError($I);

    // Get the node #.
    $shortlink = $I->grabAttributeFrom('link[rel="shortlink"]', 'href');
    $patharray = explode('/', $shortlink);
    $node = end($patharray);

    // Check that the auto menu box is still checked.
    $I->amOnPage("/node/$node/edit");
    $this->dontSeePHPError($I);
    $I->seeCheckboxIsChecked('#edit-menu-automenu');
  }

}
