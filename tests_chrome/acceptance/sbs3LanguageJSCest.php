<?php

use Page\EditSitePage;
use Page\Common;

/**
 * Tests for SBS Translation module.
 *
 * These tests cover the scenario where 3 languages are active, and are tests
 * requiring JavaScript.
 *
 * @see Sbs3LanguageCest
 */
class Sbs3LanguageJSCest {

  private $language2 = 'aa';
  private $language3 = 'bn';

  /**
   * Check for PHP warnings and notices.
   */
  private function dontSeePhpError(AcceptanceTester $I) {
    $I->dontSee('Warning:', '.messages.error');
    $I->dontSee('Notice:', '.messages.error');
  }

  /**
   * Tasks to execute before test.
   */
  public function _before(AcceptanceTester $I) {
    $this->tester = $I;

    $I->login(ADMIN_USERNAME, ADMIN_PASSWORD);
    // Add languages if necessary.
    $I->amOnPage('/admin/config/regional/language');
    $langs = $I->grabMultiple('#language-order input[type="radio"]', 'value');
    if (!in_array($this->language2, $langs)) {
      $I->amOnPage('/admin/config/regional/language/add');
      $I->selectOption('#edit-langcode', $this->language2);
      $I->click('Add language');
      $I->see('has been created and can now be used');
    }
    if (!in_array($this->language3, $langs)) {
      $I->amOnPage('/admin/config/regional/language/add');
      $I->selectOption('#edit-langcode', $this->language3);
      $I->click('Add language');
      $I->see('has been created and can now be used');
    }
    if (count($langs) > 3) {
      $I->see('Too many languages in test site. Must have only three');
    }
  }

  /**
   * Tasks to execute after test.
   */
  public function _after(AcceptanceTester $I) {
  }

  /**
   * Use the PLUpload widget to upload one file.
   *
   * Copied from wildfireEUCookieComplianceJSCest.php.
   */
  private function pluploadAddFileJs($filename) {
    $I = $this->tester;
    // Remove style to access hidden js input element.
    $I->executeJS('jQuery("div.plupload.html5").removeAttr("style")');
    $I->attachFile('//div[contains(concat(" ", normalize-space(@class), " "), " html5 ")]/input', $filename);
    $I->see($filename, 'div.plupload_file_name span');
    // Reset the style attributes that were removed.
    $I->executeJS('jQuery("div.plupload.html5").attr("style", "position: absolute; background: transparent; background-color: transparent; width: 71px; height: 19px; overflow: hidden; overflow-x: hidden; overflow-y: hidden; z-index: -1; opacity: 0; top: 173px; left: 9px")');
    $I->see('0%', '//div[contains(normalize-space(@class), "plupload_file_name")]/span[contains(normalize-space(.), "' . $filename . '")]/following::div[contains(normalize-space(@class), "plupload_file_status")]');
  }

  /**
   * Ensure the bug related to gallery creation is fixed.
   *
   * @see https://trello.com/c/2dKkFFPh/1009-460-on-a-site-with-2-languages-adding-an-image-to-a-gallery-triggers-php-notice
   */
  public function check3LanguageGalleryBugIsFixed(AcceptanceTester $I, Common $common, EditSitePage $editSite) {
    $I->wantTo('ensure that a bug that occurred when creating galleries etc is fixed');
    $rand = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 5);

    $I->amGoingTo("create a gallery page that requires a file upload");
    $I->amOnPage('/node/add/gallery');
    $common->dontSeePhpError();
    $I->fillField($editSite::$singleLanguageTitleField, "$rand-gallery");
    $this->pluploadAddFileJs('bigbuck.jpg');
    $I->click('input[value="Start upload"]');
    $I->seeElement('div.image-preview.rotate-enabled');
    $I->seeElement('a.rotate-icon.jquery-once-1-processed');
    $common->dontSeePhpError();
  }

}
