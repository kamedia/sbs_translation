<?php
use \Codeception\Util\Locator;

class sbsAutoMenuAcceptanceCest {

  public function __construct() {
    $this->random_title = $this->randomString(10);
  }

  public function _before(AcceptanceTester $I) {
    $this->languages = new \Page\WildfireLanguages($I);
    $this->commonUsers = new \Page\CommonUsers($I);
    $this->pagesOverview = new \Page\WildfireSitePagesOverview($I);
  }

  public function _after(AcceptanceTester $I) {
  }

  private function randomString($length = 6) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
    }
    return $str;
  }

  private function pathAliasForPage($page_id, $language_code = '') {
    $title = $this->random_title . '-' . $page_id . '-' . $language_code;
    return str_replace(' ', '-', strtolower($title));
  }

  public function autoMenuTest(AcceptanceTester $I) {
    $this->languages->deleteAllLanguages();
    $this->commonUsers->adminSignin();

    $I->wantTo("test auto menu with four languages");
    // Create a number of languages, each beginning with 'a' and the same random
    // string, and followed by a unique suffix.
    $random_language = 'a' . $this->randomString(4);
    $random_language_code = 'a' . strtolower($this->randomString(4));
    $new_language_suffixes = array('a', 'b', 'c', 'd',);
    $new_languages = array();
    foreach ($new_language_suffixes as $new_language_suffix) {
      $new_language_code = $random_language_code . '-' . $new_language_suffix;
      $new_language_name = $random_language . '-' . $new_language_suffix;
      $new_languages[$new_language_code] = $new_language_name;
    }
    foreach($new_languages as $new_language_code => $new_language_name) {
      $this->languages->createNewLanguage($new_language_name, $new_language_code);
    }

    $I->amGoingTo("create a page with some translations");
    // Create page in first translation language (leave English side blank).
    $I->amOnPage('/node/add/wildfire-page');
    $I->uncheckOption('#edit-path-pathauto');
    $I->uncheckOption('#edit-menu-automenu');
    $first_translation_language_name = reset($new_languages);
    $first_translation_language_code = key($new_languages);
    $first_translation_path_alias = $this->pathAliasForPage('', $first_translation_language_code);
    $I->selectOption('select[id=edit-language]', $first_translation_language_code);
    $I->fillField('#edit-title-field-und-0-value', $this->random_title . ' in ' . $first_translation_language_name);
    $I->fillField('#edit-body-und-0-value', 'test content ' . $first_translation_language_code);
    $I->fillField('#edit-path-alias', $first_translation_path_alias);
    $I->fillField('#edit-menu-link-title', $this->random_title . ' custom menu title in ' . $first_translation_language_name);

    $I->click('#edit-submit');
    $I->see(' has been created.');
    $I->dontSeeElement('.messages.error');

    // Create further translations.
    foreach ($new_languages as $translation_language_code => $translation_language_name) {
      if ($translation_language_code != $first_translation_language_code) {
        $I->amOnPage('/' . $first_translation_language_code . '/' . $first_translation_path_alias);
        // Single language method
        //~ $I->click('Translate', \Page\ViewSitePage::$translateTab);
        //~ $I->click('add', '//td[contains(text(), "' . $translation_language_name . '")]/following-sibling::td');
        // SBS method
        $I->click('Add ' . $translation_language_name, 'ul.primary');
        $I->fillField('#edit-title-field-' . $translation_language_code . '-0-value', $this->random_title . ' in ' . $translation_language_name);
        $I->fillField('#edit-body-' . $translation_language_code . '-0-value', 'test content ' . $translation_language_code);
        $I->uncheckOption('#edit-menu-automenu');
        $I->uncheckOption('#edit-path-pathauto');
        $path_alias = $this->pathAliasForPage('', $translation_language_code);
        $I->fillField('#edit-path-alias', $path_alias);
        $I->fillField('#edit-menu-link-title', $this->random_title . ' custom menu title in ' . $translation_language_name);
        $I->click('#edit-submit');
        $I->see(' has been updated.');
      }
    }

    $I->amGoingTo('check all the custom menu titles are active');
    $I->amOnPage(\Page\WildfireSitePagesOverview::$url);
    foreach ($new_languages as $translation_language_code => $translation_language_name) {
      $I->see($this->random_title . ' custom menu title in ' . $translation_language_name) ;
    }

    $I->amGoingTo('activate auto menu');
    $I->amOnPage('/' . $first_translation_language_code . '/' . $first_translation_path_alias);
    $I->click('Edit', \Page\ViewSitePage::$editTab);
    $I->checkOption('#edit-menu-automenu');
    $I->click('#edit-submit');
    $I->see(' has been updated.');
    $I->dontSeeElement('.messages.error');

    $I->amGoingTo('see that auto menu has been applied to all translations');
    $I->amOnPage(\Page\WildfireSitePagesOverview::$url);
    foreach ($new_languages as $translation_language_code => $translation_language_name) {
      $I->see($this->random_title . ' in ' . $translation_language_name);
      $I->dontSeeElement('.messages.error');
    }

    $I->amGoingTo("delete the pages I created");
    $I->amOnPage('/' . $first_translation_language_code . '/' . $first_translation_path_alias);
    $I->click('Delete');
    $I->checkOption('#edit-alltranslations');
    $I->click('#edit-submit');
    $I->see(' has been deleted.');

    $I->amGoingTo("delete the languages I created");
    foreach ($new_languages as $new_language_code => $new_language_name) {
      $this->languages->deleteLanguage($new_language_name);
    }
  }
}
