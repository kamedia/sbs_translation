<?php

class sbsAcceptanceJSCest
{

  // HOOK: before test
  public function _before(\AcceptanceTester $I, \Page\CommonUsers $commonUsers, Page\Common $common) {
    $I->login(ADMIN_USERNAME, ADMIN_PASSWORD);
  }

  // HOOK: after test
  public function _after(\AcceptanceTester $I, \Page\CommonUsers $commonUsers, Page\Common $common) {
  }

  public function checkLanguageSelectAttributeSwitching(\AcceptanceTester $I, \Page\CommonUsers $commonUsers, Page\Common $common, Page\WildfireLanguages $wildfireLanguages) {

    // For this test, language code must be only 2 letters
    $rand = 'aa';
    $randrtl = 'bb';

    $I->wantTo('Test that language selector changes lang & dir attributes and classes.');

    // createNewLanguage() can't create a RTL language and also makes a call to
    // confirmICanSee which is phpbrowser only.
    // $wildfireLanguages->createNewLanguage("$rand1-name", $rand1);

    $I->amGoingTo('Add a custom language');
    $I->amOnPage('/admin/config/regional/language/add');
    $I->click('#edit-custom-language a');
    $I->fillField('#edit-langcode--2', $rand);
    $I->fillField('#edit-name', "$rand-name");
    $I->fillField('#edit-native', "$rand-native");
    $I->fillField('#edit-prefix', "$rand-prefix");
    $I->selectOption('input[name=direction]', 0);
    $I->click('#edit-submit--2');

    $I->amGoingTo('Add a second language that is RTL so that the language selector appears');
    // The second language needs to be alphabetically AFTER the first language
    // so that it isn't already selected when the form loads.
    $I->amOnPage('/admin/config/regional/language/add');
    $I->click('#edit-custom-language a');
    $I->fillField('#edit-langcode--2', $randrtl);
    $I->fillField('#edit-name', $randrtl . '-name');
    $I->fillField('#edit-native', $randrtl . '-native');
    $I->fillField('#edit-prefix', $randrtl . '-prefix');
    $I->selectOption('input[name=direction]', 1);
    $I->click('#edit-submit--2');
    // $I->makeScreenshot();

    $I->amGoingTo('Create a new page and test that the language selector changes the attributes.');
    $I->amOnPage('/node/add/wildfire-page');
    $I->wait(1); // wait for ckeditor to load
    $I->amGoingTo('Check that right side language starts as LTR language');
    $I->seeElement('#edit-title-field-und-0-value', ['lang' => $rand], ['dir' => 'ltr']);
    $I->selectOption('#edit-language', $randrtl .'-name');
    $I->uncheckOption('#edit-menu-automenu');
    $I->uncheckOption('#edit-path-pathauto');
    // $I->makeScreenshot();
    $I->amGoingTo('Check that right side language has switched to RTL language');
    $I->seeElement('#edit-title-field-und-0-value', ['lang' => $randrtl], ['dir' => 'rtl']);
    $I->seeElement('#edit-menu-link-title', ['lang' => $randrtl], ['dir' => 'rtl']);
    $I->seeElement('#edit-path-alias', ['lang' => $randrtl], ['dir' => 'rtl']);

    $I->amGoingTo('Test that right side body and summary textarea receive the new lang and dir attributes');
    // Body content textarea is a nojs fallback that is hidden and replaced with
    // ckeditor.
    // $I->executeJS('jQuery("#edit-body-en-0-value").show().css("visibility", "visible")');
    // $I->executeJS('jQuery("#edit-body-und-0-value").show().css("visibility", "visible")');
    // $I->executeJS('jQuery("#edit-body-en-0-summary").show().css("visibility", "visible")');
    // $I->executeJS('jQuery("#edit-body-und-0-summary").show().css("visibility", "visible")');
    // $I->seeElement('#edit-body-en-0-value', ['lang' => 'en'], ['dir' => 'ltr']);
    // $I->seeElement('#edit-body-und-0-value', ['lang' => $rand], ['dir' => 'rtl']);
    // $I->seeElement('#edit-body-en-0-summary', ['lang' => 'en'], ['dir' => 'ltr']);
    // $I->seeElement('#edit-body-und-0-summary', ['lang' => $rand], ['dir' => 'rtl']);
    // Changed to seeInSource because it's simpler and doesn't involve js to make it visible.
    $I->seeInSource('name="body[und][0][value]" cols="60" rows="20" lang="' . $randrtl . '" dir="rtl"');
    $I->seeInSource('name="body[und][0][summary]" cols="60" rows="3" lang="' . $randrtl . '" dir="rtl"');

    $I->amGoingTo('Test that metatag title and description the new lang and dir attributes');
    // $I->click('#edit-sbsmetatag .left a.fieldset-title');
    // $I->click('#edit-sbsmetatag .right a.fieldset-title');
    // $I->seeElement('#edit-metatags-en-title-value', ['lang' => 'en'], ['dir' => 'ltr']);
    // $I->seeElement('#edit-metatags-und-title-value', ['lang' => $rand], ['dir' => 'rtl']);
    // $I->seeElement('#edit-metatags-en-description-value', ['lang' => 'en'], ['dir' => 'ltr']);
    // $I->seeElement('#edit-metatags-und-description-value', ['lang' => $rand], ['dir' => 'rtl']);
    // Changed to seeInSource because I had trouble getting both metatag fields visible.
    $I->seeInSource('name="metatags[und][title][value]" value="[node:title_field] | [site:name]" size="60" maxlength="1024" class="form-text" lang="' . $randrtl . '" dir="rtl"');
    $I->seeInSource('name="metatags[und][description][value]" cols="60" rows="2" class="form-textarea" lang="' . $randrtl . '" dir="rtl"');

    // Assign a name to each iframe so we can switch to it.
    $I->executeJS('jQuery("#cke_edit-body-en-0-value iframe").attr("name", "left_ckeditor")');
    $I->executeJS('jQuery("#cke_edit-body-und-0-value iframe").attr("name", "right_ckeditor")');

    $I->switchToIFrame('left_ckeditor');
    $I->seeElement('html', ['lang' => 'en'], ['dir' => 'ltr']);
    $I->seeElement('body.cke_contents_ltr');
    $I->switchToIFrame();

    $I->switchToIFrame('right_ckeditor');
    $I->seeElement('html', ['lang' => $randrtl], ['dir' => 'rtl']);
    $I->seeElement('body.cke_contents_rtl');
    // I couldn't get this to pass ???
    // $I->seeElement('body.wildfire-font-language-' . $randrtl);
    $I->switchToIFrame();

    $common->dontSeePHPError();
  }
}
